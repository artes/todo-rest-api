from rest_framework import serializers
from api.models import Task, Tag, User


class TaskSerializer(serializers.ModelSerializer):
    parent_task = serializers.PrimaryKeyRelatedField(allow_null=True, source='self')
    tags = serializers.PrimaryKeyRelatedField(many=True)

    class Meta:
        model = Task
        fields = ('id', 'title', 'description', 'status', 'created_time', 'updated_time', 'parent_task', 'tags')


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'title', 'is_system_tag')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email')