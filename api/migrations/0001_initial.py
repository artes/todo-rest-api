# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-30 14:34
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AuthSession',
            fields=[
                ('token', models.CharField(db_index=True, max_length=200, primary_key=True, serialize=False, unique=True)),
                ('create_time', models.DateTimeField(db_index=True, default=datetime.datetime.now)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=250, verbose_name='Tag title')),
                ('is_system_tag', models.BooleanField(default=False, verbose_name='Is the tag was created by system or by user')),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=250, verbose_name='Task title')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Task short description')),
                ('status', models.BooleanField(default=False, verbose_name='Task status')),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('updated_time', models.DateTimeField(auto_now=True)),
                ('parent_task', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Task')),
                ('task_tags', models.ManyToManyField(to='api.Tag')),
            ],
            options={
                'ordering': ('created_time',),
            },
        ),
    ]
