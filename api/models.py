from __future__ import unicode_literals
from uuid import UUID
from OpenSSL import rand

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from datetime import datetime, timedelta


class Tag(models.Model):
    title = models.CharField(_('Tag title'), max_length=250)
    is_system_tag = models.BooleanField(_('Is the tag was created by system or by user'), default=False)


class Task(models.Model):
    title = models.CharField(_('Task title'), max_length=250, db_index=True)
    description = models.TextField(_('Task short description'), null=True, blank=True)
    status = models.BooleanField(_('Task status'), default=False)
    created_time = models.DateTimeField(_('Task status'), auto_now_add=True)
    updated_time = models.DateTimeField(auto_now=True)
    start_date = models.DateTimeField(_('Task start date'), default=datetime.now())
    due_date = models.DateTimeField(default=datetime.now()+timedelta(hours=24))
    parent_task = models.ForeignKey('self', null=True, blank=True)
    tags = models.ManyToManyField(Tag)

    class Meta:
        ordering = ('created_time',)


class AuthSession(models.Model):
    """
    Authentication session system
    """
    token = models.CharField(max_length=200, unique=True, db_index=True, primary_key=True)
    user = models.ForeignKey(User)
    create_time = models.DateTimeField(db_index=True, default=datetime.now)

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = self.generate_token()
        return super(AuthSession, self).save(*args, **kwargs)

    def generate_token(self):
        return UUID(bytes=rand.bytes(16))
